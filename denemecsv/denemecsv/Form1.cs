﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace denemecsv
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }
        string[] derger1;
        string[] derger2;
        string toplam = "";
        List<string> _dizi = new List<string>();
        List<string> _dizi2 = new List<string>();
        int bulunan = 0;
        int b = 0;
        private void btn1_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Excel Dosyası |*.csv";
            file.RestoreDirectory = true;
            file.Title = "Excel Dosyası Seçiniz..";
            file.Multiselect = false;

            if (file.ShowDialog() == DialogResult.OK)
            {
                string dosyayolu1 = file.FileName;
                System.IO.StreamReader dosya1 = new System.IO.StreamReader(dosyayolu1, Encoding.Default);
                string Okunan = dosya1.ReadToEnd();
                derger1 = Okunan.Split('\n');
                foreach (string item in derger1)
                { _dizi.Add(item); }
                dosya1.Close();
            }
        }
        private void btn2_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Excel Dosyası |*.csv";
            file.RestoreDirectory = true;
            file.Title = "Excel Dosyası Seçiniz..";
            file.Multiselect = false;

            if (file.ShowDialog() == DialogResult.OK)
            {
                string dosyayolu2 = file.FileName;
                System.IO.StreamReader dosya2 = new System.IO.StreamReader(dosyayolu2, Encoding.Default);
                string Okunan2 = dosya2.ReadToEnd();
                derger2 = Okunan2.Split('\n');
                foreach (string item in derger2)
                { _dizi2.Add(item); }
                dosya2.Close();
            }
        }
        private void btn3_Click_1(object sender, EventArgs e)
        {
            var dosya1uzunluk = _dizi.Count;
            var dosya2uzunluk = _dizi2.Count;
            if (_dizi[dosya1uzunluk-1] == "")
            { _dizi.RemoveAt(dosya1uzunluk-1); }
            else if (_dizi2[dosya2uzunluk-1] == "")
            { _dizi2.RemoveAt(dosya2uzunluk-1); }
            for (int i = 0; i < dosya1uzunluk; i++)
            {
                b = b+1;
                label1.Text = (b).ToString();
                for (int a = 0; a < dosya2uzunluk; a++)
                {

                    if (_dizi[i] == _dizi2[a])
                    {
                        toplam += _dizi[i].ToString();
                        _dizi.RemoveAt(i);
                        _dizi2.RemoveAt(a);
                        dosya1uzunluk = _dizi.Count;
                        dosya2uzunluk = _dizi2.Count;
                        --a;
                        --i;
                        bulunan = bulunan + 1;
                        label4.Text = bulunan.ToString();
                        break;
                    }
                    Application.DoEvents();
                }
            }
            SaveFileDialog kaydet = new SaveFileDialog();
            kaydet.Title = "Save";
            kaydet.Filter = "Excel Dosyaları (*.csv)|*.csv";
            StreamWriter writer = null;
            if (kaydet.ShowDialog() == DialogResult.OK)
            {
                writer = new StreamWriter(kaydet.FileName);
                writer.WriteLine(toplam);
                writer.Close();
            }
            MessageBox.Show("Kontrol işlemleri bitmiştir.Dosyanızı seçtiğiniz yerden alabilirsiniz.", "Csv bitiş bildirimi");

        }
    }
}
